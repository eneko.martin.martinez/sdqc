"""
Tests for the general functioning of the library
"""
import pytest
import sdqc
from pathlib import Path


def test_invalid_value():
    """
    test_invalid_value for loadinf external objects
    """

    with pytest.raises(ValueError,
                       match="Source must be a file_name, a Path, a "
                       "dictionary or a list of initialized pysd External "
                       "objects."):
        sdqc.check(['list_not_supported'])


def test_reading_error():
    """
    test_invalid_value for loading external objects
    """
    from sdqc.loading_examples import read_example_data

    path = Path(__file__).parent.joinpath('jsons',
                                          'reading_error',
                                          'reading_error.json')

    ext_dict = read_example_data(path)

    # Load from json file
    with pytest.warns(UserWarning) as ws:
        sdqc.check(ext_dict,
                   verbose=True)

    # pytest captures UserWarning and DeprecationWarning, so we need to filter
    ws = [w for w in ws if w._category_name == 'UserWarning']
    assert len(ws) == 2
    assert all([word in ws[0].message.args[0] for word in [
        "The cellrange name", "Doesn't exist in:\n"]])
    assert "The sheet doesn't exist" in ws[1].message.args[0]


def test_simple_json():
    """
    test_invalid_value for several passing test
    """
    from sdqc.loading_examples import read_example_data

    path = Path(__file__).parent.joinpath('jsons',
                                          'simple_json')

    ext_dict = read_example_data(path.joinpath('simple_json.json'))

    # Load from dict file
    objs = sdqc.check(ext_dict, path.joinpath('simple_json.ini'))

    assert objs.empty

    # Load from dict file using verbose=True
    objs = sdqc.check(ext_dict,
                      path.joinpath('simple_json.ini'),
                      verbose=True)

    assert set(objs['check_name'].values)\
        == set(['missing_values_series', 'missing_values_data',
                'outlier_values', 'series_monotony',
                'series_range', 'series_increment_type'])

    assert all(objs['check_pass'].values)


def test_missing_values():
    """
    test_missing_values using an mdl an py
    """
    from sdqc.reports import Report

    path = Path(__file__).parent.joinpath('models',
                                          'missing_values')

    # Load from mdl file
    warning_message = "Removing the columns corresponding to missing values " \
                      "in the series. Activate the missing_values check to " \
                      "see the results of the check."

    with pytest.warns(UserWarning, match=warning_message):
        objs = sdqc.check(path.joinpath('missing_values.mdl'),
                          path.joinpath('missing_values_deactivated.ini'))

    # Load from mdl file
    objs = sdqc.check(path.joinpath('missing_values.mdl'),
                      path.joinpath('missing_values.ini'))

    assert len(objs) == 14
    assert not objs[(objs['check_target'] == 'dataseries') &
                    (objs['py_name'] == '_ext_data_data_interp1')][
                        'check_pass'].values[0]
    assert objs[(objs['check_target'] == 'dataseries') &
                (objs['py_short_name'] == 'data_interp1')][
                    'check_out'].values[0] == [[4, 9],
                                               [3., 6., 10., 11.]]
    assert '_ext_constant_constant' not in objs['py_name'].values
    assert not objs[(objs['check_target'] == 'series') &
                    (objs['py_short_name'] == 'data_hb3')][
                        'check_pass'].values[0]

    # Load from py file using verbose=True
    objs = sdqc.check(path.joinpath('missing_values.py'),
                      path.joinpath('missing_values.ini'),
                      verbose=True)

    assert len(objs.index) == 29
    assert not objs[(objs['check_target'] == 'dataseries') &
                    (objs['py_name'] == '_ext_data_data_interp1')][
                        'check_pass'].values[0]
    assert objs[(objs['check_target'] == 'dataseries') &
                (objs['py_short_name'] == 'data_interp1')][
                    'check_out'].values[0] == [[4, 9], [3., 6., 10., 11.]]
    assert '_ext_constant_constant' in objs['py_name'].values
    assert objs[objs['original_name'] == 'constant']['check_pass'].values[0]
    assert objs[(objs['check_target'] == 'series') &
                (objs['original_name'] == 'data hb1')]['check_pass'].values[0]
    assert not objs[(objs['check_target'] == 'series') &
                    (objs['py_short_name'] == 'data_hb3')][
                        'check_pass'].values[0]

    report_config_path = Path(__file__).parent.parent.joinpath(
        'sdqc',
        'default-report-conf.json')

    # check that output is a Report object
    with pytest.warns(UserWarning):
        objs = sdqc.check(path.joinpath('missing_values.py'),
                          path.joinpath('missing_values.ini'),
                          report_config=report_config_path,
                          output='report')

    assert isinstance(objs, Report)


def test_non_monotonous_series():
    """
    test_non_monotonous_series using a json
    """
    from sdqc.loading_examples import read_example_data

    path = Path(__file__).parent.joinpath('jsons', 'non_monotonous')

    ext_dict = read_example_data(
        path.joinpath('non_monotonous.json'))

    # Load from py file
    objs = sdqc.check(ext_dict, path.joinpath('non_monotonous.ini'))

    assert len(objs.index) == 2
    assert not any(objs['check_pass'].values)
    assert all([name == 'series_monotony'
                for name in objs['check_name'].values])
    assert set(['Excel DaTa2', 'Excel DaTa3'])\
        == set(objs['original_name'].values)
