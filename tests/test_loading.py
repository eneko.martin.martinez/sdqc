"""
Tests for loading functions
"""
from distutils import extension
import pytest
import xarray as xr
from pathlib import Path


def test_load_data_invalid_path():
    """
    test_load
    Load a model using .mdl using PySD and model .py file
    """
    from sdqc.loading import load_data

    invalid_extension_path = Path(__file__).parent / 'models/missing_values/' \
                                                     'missing_values.py2'

    with pytest.raises(ValueError, match=r"Invalid file extension..*"):
        load_data(invalid_extension_path)

@pytest.mark.parametrize(
    "model_extension,externals",
    [
        ('.mdl', None),
        ('.py', None),
        ('.mdl', "externals.nc"),
        ('.py', "externals.nc")
    ],
    ids=["py", "mdl", "py_externals", "mdl_externals"])
def test_load_data_externals(tmp_path, model_extension, externals):
    """
    test_load
    Load a model using .mdl using PySD and model .py file
    """
    from sdqc.loading import load_data
    import pysd

    model_path = Path(__file__).parent.joinpath(
        'models/missing_values/missing_values' + model_extension)

    if externals:
        externals = tmp_path / externals
        if model_extension == ".py":
            model = pysd.load(model_path, initialize=False)
        else:
            model = pysd.read_vensim(model_path, initialize=False)
        model.serialize_externals(export_path=externals,
                                  include_externals="all",
                                  exclude_externals=None)
    # Load from mdl file
    objs = load_data(model_path, externals=externals)
    assert all([isinstance(obj, pysd.external.External) for obj in objs])


def test_load_config_default():
    """
    test_load_config_default
    Read default configuration
    """
    from sdqc.loading import load_config
    from numpy import inf
    config = load_config(None)

    assert isinstance(config, dict)
    assert config['all']['missing']
    assert not config['all']['outliers']
    assert config['all']['outliers_method'] == 'std'
    assert config['all']['outliers_nstd'] == 2
    assert config['all']['outliers_niqr'] == 1.5
    assert not config['all']['series_increment']
    assert config['all']['series_increment_type'] == "linear"
    assert not config['all']['series_range']
    assert config['all']['series_range_values'] == [-inf, inf]


def test_load_config_user():
    """
    test_load_config_user
    Read user configuration from conf1.ini
    """
    from sdqc.loading import load_config
    conf_path = Path(__file__).parent / 'confs/conf1.ini'

    with pytest.warns(UserWarning) as ws:
        config = load_config(str(conf_path))

    assert len(ws) == 1
    # check that the message matches
    assert ws[0].message.args[0].endswith(
        "defined in the config file is not used in any check")

    assert isinstance(config, dict)
    assert config['all']['missing']
    assert config['all']['outliers']
    assert config['all']['outliers_nstd'] == 2
    assert not config['dataseries']['missing']
    assert config['dataseries']['series_increment']
    assert config['dataseries']['series_increment_type'] == "exponential"
    assert config['dataseries']['series_range_values'] == [0, 100]
    assert config['constant']['missing']
    assert config['constant']['outliers_nstd'] == 2.5
    assert config['_ext_data_data_hb1']['series_range_values'] == [2, 50]


def test_load_config_not_found():
    """
    test_load_config_not_found
    Read user configuration from conf1.ini
    """
    from sdqc.loading import load_config
    with pytest.raises(FileNotFoundError) as excinfo:
        load_config('non-existent.ini')

    assert "No such file or directory: 'non-existent.ini'"\
        in str(excinfo.value)


def test_load_data_from_simple_json():
    """
    test_load_data_from_simple_json
    Load a model using .json
    """
    from sdqc.loading import load_from_dict
    from sdqc.loading_examples import read_example_data
    from pysd import external

    # Load all externals from json

    path_json = Path(__file__).parent / 'jsons/simple_json/simple_json.json'
    objs = load_from_dict(
        read_example_data(path_json))

    assert all([isinstance(obj, external.External) for obj in objs])

    assert len(objs) == 2

    for obj in objs:
        if obj.py_short_name == 'excel_constant':
            assert obj() == 3
        if obj.py_short_name == 'excel_data':
            assert obj(15) == 1.5
            assert obj(40) == -1

    # Load new externals from json
    path_diff_json = Path(__file__).parent / 'jsons/simple_json/diff.json'

    objs = load_from_dict(
        read_example_data(path_json, path_diff_json))

    assert all([isinstance(obj, external.External) for obj in objs])

    assert len(objs) == 1
    assert objs[0]() == 3


def test_load_data_from_subscript_json():
    """
    test_load_data_from_subscript_json
    Load a model using .json
    """
    from sdqc.loading import load_from_dict
    from sdqc.loading_examples import read_example_data
    from pysd import external

    # Load all externals from json
    path_subscript_json = Path(__file__).parent / 'jsons/subscript_json/' \
                                                  'subscript_json.json'
    objs = load_from_dict(
        read_example_data(path_subscript_json))

    assert all([isinstance(obj, external.External) for obj in objs])

    expected = {}

    expected['excel_constant'] = xr.DataArray(
        [[3, 2, 35], [21, 1, 4]],
        {'DIM1': ['A', 'B'], 'DIM2': ['C', 'D', 'E']},
        ['DIM1', 'DIM2'])

    expected['excel_constant2'] = xr.DataArray(
        [[[3, 1],
          [2, 45],
          [35, 43]],
         [[21, 3],
          [1, 34],
          [4, 10]]],
        {'DIM1': ['A', 'B'], 'DIM2': ['C', 'D', 'E'], 'DIM3': ['X', 'Y']},
        ['DIM1', 'DIM2', 'DIM3'])

    expected['excel_data'] = xr.DataArray(
        [[[6, 11, 16],
          [21, 26, 31]],
         [[7, 12, 17],
          [22., 27, 32]],
         [[8, 13, 18],
          [23, 28, 33]],
         [[9, 14, 19],
          [24, 29, 34]],
         [[10, 15, 20],
          [25, 30, 35]]],
        {'DIM1': ['A', 'B'], 'DIM2': ['C', 'D', 'E'],
         'lookup_dim': [1., 2., 4., 10., 20.]},
        ['lookup_dim', 'DIM1', 'DIM2'])

    assert len(objs) == 3
    for obj in objs:
        assert obj.data.equals(expected[obj.py_short_name])
