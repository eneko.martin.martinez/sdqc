"""
Tests for checks
"""
import pytest

import numpy as np
import xarray as xr


def test_missing_values():
    from sdqc.checks import missing_values

    dataNone = xr.DataArray([[1, 2], [3, 5]])
    data2 = np.array([[np.nan, 2], [np.nan, 5]])
    data3 = np.array([[np.nan, 2, np.nan], [34, np.nan, 5]])

    assert missing_values(dataNone) == (True, [0, 4])
    assert missing_values(data2) == (False, [2, 4])
    assert missing_values(data3) == (False, [3, 6])


def test_missing_values_data():
    from sdqc.checks import missing_values_data

    series = np.array([2000, 2010, 2020, 2030])
    datafull = np.array([[3, 4], [2, 5], [3, 2], [5, 6]])
    data2 = np.array([[3, 4], [2, 5], [3, 2], [np.nan, 6]])
    data3 = np.array([[3, 4], [2, 5], [3, 2], [np.nan, np.nan]])
    data4 = np.array([[3, 4], [2, 5], [np.nan, np.nan], [np.nan, np.nan]])
    data5 = np.array([2, np.nan, 4, np.nan])

    # all missing values
    assert missing_values_data(series, series, 'all')\
        == (True, [[0, 4], []])
    assert missing_values_data(datafull, series, 'all')\
        == (True, [[0, 4], []])
    assert missing_values_data(data2, series, 'all')\
        == (True, [[0, 4], []])
    assert missing_values_data(data3, series, 'all')\
        == (False, [[1, 4], [2030]])
    assert missing_values_data(data4, series, 'all')\
        == (False, [[2, 4], [2020, 2030]])
    assert missing_values_data(data5, series, 'all')\
        == (False, [[2, 4], [2010, 2030]])

    # any missing values
    assert missing_values_data(series, series, 'any')\
        == (True, [[0, 4], []])
    assert missing_values_data(datafull, series, 'any')\
        == (True, [[0, 8], []])
    assert missing_values_data(data2, series, 'any')\
        == (False, [[1, 8], [2030]])
    assert missing_values_data(data3, series, 'any')\
        == (False, [[2, 8], [2030]])
    assert missing_values_data(data4, series, 'any')\
        == (False, [[4, 8], [2020, 2030]])
    assert missing_values_data(data5, series, 'any')\
        == (False, [[2, 4], [2010, 2030]])

    # non valid missing
    with pytest.raises(ValueError,
                       match="Invalid value for completeness argument."):
        missing_values_data(data3, series, 'NA')


def test_series_monotony():
    from sdqc.checks import series_monotony

    datagrow = np.array([0, 2, 3, 5])
    datagrow2 = np.array([2000, 2010, 2020, 2030])
    data2 = np.array([3, 2, 3, 5])
    data3 = np.array([5, 4, 3, 2, 1])
    data4 = np.array([3, 2, 1, 5])

    assert series_monotony(datagrow) == (True, [0, 5])
    assert series_monotony(datagrow2) == (True, [2000, 2030])
    assert series_monotony(data2) == (False, [2, 5])
    assert series_monotony(data3) == (True, [5, 1])
    assert series_monotony(data4) == (False, [1, 5])


def test_series_range():
    from sdqc.checks import series_range

    datagrow = np.array([0, 2, 3, 5])
    data2 = np.array([3, 2, 5])
    data3 = np.array([5, 4, 3, 2, 1])

    assert series_range(datagrow, [0, 5]) == (True, [[0, 5], [0, 5]])
    assert series_range(datagrow, [1, 5]) == (False, [[0, 5], [1, 5]])
    assert series_range(datagrow, [0, 3]) == (False, [[0, 5], [0, 3]])
    assert series_range(data2, [2, 5]) == (True, [[2, 5], [2, 5]])
    assert series_range(data3, [0, 10]) == (True, [[1, 5], [0, 10]])
    assert series_range(data3, [2, 10]) == (False, [[1, 5], [2, 10]])


def test_series_increment_type():
    from sdqc.checks import series_increment_type

    datalinear1 = np.array([10, 20, 30, 40])
    datalinear2 = np.array([100, 50, 0])
    datanolinear1 = np.array([1, 2, 3, 5])
    datanolinear2 = np.array([50, 40, 30, 20, 1])

    assert series_increment_type(datalinear1, 'linear')\
        == (True, [10])
    assert series_increment_type(datalinear2, 'linear')\
        == (True, [-50])
    assert series_increment_type(datanolinear1, 'linear')\
        == (False, list(datanolinear1))
    assert series_increment_type(datanolinear2, 'linear')\
        == (False, list(datanolinear2))

    # non-valid type
    with pytest.raises(ValueError,
                       match="The argument non_valid_type is not supported"
                             + " by series_increment_type."):
        series_increment_type(datalinear1, 'non_valid_type')


def test_outlier_values():
    from sdqc.checks import outlier_values

    n = int(10)
    np.random.seed(123)
    data1 = np.random.normal(5, 1.5, n)

    # outliers from a normal distribution
    assert outlier_values(data1, 'std', nstd=1.0) == (False,
                                                      [[data1[1], data1[3],
                                                        data1[5], data1[6],
                                                        data1[8]],
                                                       n])
    assert outlier_values(data1, 'std', nstd=1.5) == (False,
                                                      [[data1[5], data1[6]],
                                                       n])
    assert outlier_values(data1, 'std', nstd=2) == (True, [[], n])

    assert outlier_values(data1, 'iqr', niqr=0.5) == (False, [data1[6], n])
    assert outlier_values(data1, 'iqr', niqr=1.0) == (True, [[], n])

    # data loaded from two different files/tabs or cellrange names
    data2 = np.random.normal(5, 1.5, (2, 5))
    data2[1, 4] = 15
    coords = [{"dim1": ["A1"], "dim2": ["B1", "B2", "B3", "B4", "B5"]},
              {"dim1": ["A2"], "dim2": ["B1", "B2", "B3", "B4", "B5"]}]
    final_coords = {"dim1": ["A1", "A2"],
                    "dim2": ["B1", "B2", "B3", "B4", "B5"]}
    assert outlier_values(data2, 'std', nstd=2,
                          coords=coords,
                          final_coords=final_coords) == (False,
                                                         [[[], [data2[1, 4]]],
                                                          n])
    # non-valid type
    with pytest.raises(ValueError,
                       match="The method non_valid_method is not "
                             + "supported by outlier_values."):
        outlier_values(data1, 'non_valid_method')
