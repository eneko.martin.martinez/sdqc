"""
Tests for tools
"""


def test_add_config_to_ext():
    from pysd import external
    from sdqc.tools import _add_config_to_ext

    extL = external.ExtLookup("", "", "", "B5", {}, "", {}, "_ext_lookup_obj1")
    extL.py_short_name = "obj1"
    extL.original_name = "Obj1$"

    extD = external.ExtData("", "", "", "C9", None, {}, "", {},
                            "_ext_data_obj2")
    extD.py_short_name = "obj2"
    extD.original_name = "obj2$"

    extC = external.ExtConstant("", "", "C8", {}, "", {}, "_ext_constant_obj3")
    extC.py_short_name = "obj3"
    extC.original_name = "OBJ3$"

    config = {
        'all': {},
        'dataseries': {
            'missing': True,
            'outliers': False,
            'outliers_method': 'std',
            'series_range': False,
            'series_range_values': [0, 10],
            'series_increment': False,
            'series_increment_type': 'exponential'
        },
        'constant': {
            'missing': False,
            'outliers': True,
            'outliers_method': 'std',
            'outliers_nstd': 2
        },
        'obj1': {
            'missing': False,
            'series_increment': True,
            'series_increment_type': 'linear'
        },
        '_ext_data_obj2': {
            'series_range': True,
            'outliers': True,
            'outliers_nstd': 1
        },
        'obj3$': {
            'outliers_method': 'iqr',
            'outliers_niqr': 3
        }
        }

    _add_config_to_ext([extL, extD, extC], config)

    assert extL.check_config == {
            '_type': 'dataseries',
            'missing': False,
            'outliers': False,
            'outliers_method': 'std',
            'series_range': False,
            'series_range_values': [0, 10],
            'series_increment': True,
            'series_increment_type': 'linear'
        }

    assert extD.check_config == {
            '_type': 'dataseries',
            'missing': True,
            'series_range': True,
            'outliers': True,
            'outliers_method': 'std',
            'outliers_nstd': 1,
            'series_range_values': [0, 10],
            'series_increment': False,
            'series_increment_type': 'exponential'
        }

    assert extC.check_config == {
            '_type': 'constant',
            'missing': False,
            'outliers': True,
            'outliers_method': 'iqr',
            'outliers_nstd': 2,
            'outliers_niqr': 3
        }
