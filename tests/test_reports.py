from pathlib import Path
import pytest
from sdqc.reports import (MissingValuesDataHandler, MissingValuesHandler,
                          MissingValuesSeriesHandler, OutlierValuesHandler)


class TestCheck:
    from sdqc.reports import Severity

    @pytest.mark.parametrize("severity", ["WARNING", "ERROR", "CRITICAL"])
    def test_display_severity_string(self, severity):
        from sdqc.reports import Check

        # severity as a string does return the same value
        message = "test"
        check_name = "series_range"

        obj = Check(severity, message, check_name)
        assert obj.display_severity == severity

    @pytest.mark.parametrize("severity,expected",
                             [(Severity.WARNING, "WARNING"),
                              (Severity.ERROR, "ERROR"),
                              (Severity.CRITICAL, "CRITICAL")],
                             ids=["WARNING", "ERROR", "CRITICAL"])
    def test_display_severity_severity_object(self, severity, expected):
        from sdqc.reports import Check

        message = "test"
        check_name = "series_range"

        obj = Check(severity, message, check_name)
        assert obj.display_severity == expected

    @pytest.mark.parametrize("severity", ["DANGER!", 1])
    def test_display_severity_invalid_type(self, severity):
        from sdqc.reports import Check

        message = "test"
        check_name = "series_range"

        with pytest.raises(ValueError) as err:
            Check(severity, message, check_name)

        assert str(err.value).startswith(f"{check_name} only accepts: ")

    def test_update_value(self):
        from sdqc.reports import Check, Severity
        message = "test"
        check_name = "series_range"
        severity1 = "WARNING"
        severity2 = Severity.ERROR

        obj = Check(severity1, message, check_name)
        assert obj.display_severity == "WARNING"

        obj.update_value(severity2)

        assert obj.display_severity == "ERROR"

    def test_invalid_severity(self):
        from sdqc.reports import Check, Severity

        severity = "WARNIG!!!!"
        message = "test"
        check_name = "series_range"

        with pytest.raises(ValueError) as err:
            Check(severity, message, check_name)

        assert str(err.value) == "{} only accepts:  {}".format(
            check_name,
            ', '.join([x.name for x in Severity]))


class TestCheckThresholds:
    from sdqc.reports import Severity

    @pytest.mark.parametrize("threshold", ["WARNING", 1])
    def test_invalid_thresholds_type(self, threshold):

        from sdqc.reports import CheckThresholds

        message = "{}"
        check_name = "series_range"
        handler = MissingValuesHandler

        with pytest.raises(TypeError) as err:
            CheckThresholds(threshold, message, handler, check_name)

        assert str(err.value) == f"{check_name} only accepts dictionaries."

    def test_update_value(self):
        from sdqc.reports import CheckThresholds, MissingValuesHandler

        message = "{}"
        check_name = "series_range"
        thresholds1 = {"WARNING": 10, "ERROR": 20, "CRITICAL": 30}
        thresholds2 = {"WARNING": 20, "ERROR": 30, "CRITICAL": 40}
        handler = MissingValuesHandler

        obj = CheckThresholds(thresholds1, message, handler, check_name)
        obj.update_value(thresholds2)

        assert obj.thresholds == thresholds2

    @pytest.mark.parametrize("percent_fails,expected",
                             [(5, Severity.WARNING),
                              (30, Severity.ERROR),
                              (60, Severity.CRITICAL)])
    def test_get_severity(self, percent_fails, expected):
        from sdqc.reports import CheckThresholds

        message = "{}"
        check_name = "series_range"
        thresholds = {"WARNING": 20, "ERROR": 25, "CRITICAL": 40}
        handler = MissingValuesHandler

        obj = CheckThresholds(thresholds, message, handler, check_name)
        assert obj.get_severity(percent_fails) == expected

    @pytest.mark.parametrize(
        "handler,input,num_messages,expected",
        [(MissingValuesHandler, [1, 10], 2, "1,10.0"),
         (MissingValuesSeriesHandler, [1, 10], 2, "1,10.0"),
         (MissingValuesDataHandler, [[2, 20], [30, 40]], 3, "2,10.0,[30, 40]"),
         (OutlierValuesHandler, [[0.1], 10], 3, "1,10.0,[0.1]"),
         (OutlierValuesHandler, [[[0.1], []], 10], 3, "1,10.0,[[0.1], []]")])
    def test_update_severity_and_message(self, handler, input, num_messages,
                                         expected):
        from sdqc.reports import CheckThresholds

        # generating the required number of text input spaces
        field = "{}"
        message = ",".join([field] * num_messages)

        check_name = "series_range"
        thresholds = {"WARNING": 20, "ERROR": 25, "CRITICAL": 40}

        obj = CheckThresholds(thresholds, message, handler, check_name)
        obj.update_severity_and_message(input)
        assert obj.display_severity == "WARNING"
        assert obj.message == expected


class TestSeverity:

    def test_objects(self):
        from sdqc.reports import Severity

        for severity in Severity:
            assert severity.name in ["WARNING", "ERROR", "CRITICAL"]


class TestReport:

    def test_empty_data_checks_dict(self):

        from sdqc.reports import Report

        # empty checks result
        out = {}
        message = "Nothing to report. The dataframe is empty."
        with pytest.warns(UserWarning, match=message):
            Report(out)

    def test_invalid_report_config_file(self, tmpdir):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        # invalid json key
        json_config = tmpdir.join("hello.json")
        json_config.write("hello: world")

        with pytest.raises(ValueError) as err1:
            report.build_report(Path(json_config), verbose=True)

        # passing a config dict instead of a Path or str
        with pytest.raises(ValueError) as err2:
            report.build_report({"foo": "bar"}, verbose=False)

        assert str(err1.value) == "Report configuration file not correctly "\
                                  "formatted."
        assert str(err2.value) == "Invalid key in report configuration (foo)."

    def test_invalid_check_target(self):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["AAA"]}

        with pytest.raises(ValueError, match="Invalid check_target 'AAA'"):
            Report(out)

    def test_default_config_file(self):
        from sdqc.reports import Report
        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)
        # we are not testing the KeyError, but the fact that not passing a
        # report config file will show a warning
        with pytest.raises(KeyError):
            with pytest.warns(UserWarning) as ws:
                report.build_report(verbose=False)

        ws = [w for w in ws if w._category_name == 'UserWarning']
        assert len(ws) == 1
        assert ws[0].message.args[0] == "No report configuration provided. " \
                                        "Using default."

    def test__validate_report_configuration(self):

        from sdqc.reports import Report
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        # thresholds in wrong order
        report.report_config = {"missing_values_series": {"WARNING": 20,
                                                          "ERROR": 15,
                                                          "CRITICAL": 50
                                                          }}

        with pytest.raises(ValueError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value).startswith(
            "missing_values_series thresholds must be defined in "
            "ascending order") is True

        # thresholds must be above 0 and below 100
        report.report_config = {"missing_values_series": {"WARNING": 200,
                                                          "ERROR": 15,
                                                          "CRITICAL": 50
                                                          }}

        with pytest.raises(ValueError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value) == "missing_values_series thresholds must be " \
                                 "between 0 and 100."

        # invalid check name
        report.report_config = {"foobar": {}}

        with pytest.raises(ValueError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value).startswith(
            "Invalid key in report configuration") is True

        # wrong type for check name
        report.report_config = {"missing_values_series": "WARNING"}

        with pytest.raises(ValueError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value).startswith(
            "missing_values_series in the report configuration file should be"
            ) is True

        # wrong severity name
        report.report_config = {"missing_values_series": {"BAR": 20,
                                                          "FOO": 15,
                                                          "FOOBAR": 50
                                                          }}

        with pytest.raises(ValueError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value) == "Invalid severity name for " \
                                 "missing_values_series."

        # wrong severity threshold type
        report.report_config = {"missing_values_series": {"WARNING": "foo",
                                                          "ERROR": 15,
                                                          "CRITICAL": 50
                                                          }}

        with pytest.raises(TypeError) as err:
            report._Report__validate_report_configuration()

        assert str(err.value) == "missing_values_series thresholds must be " \
                                 "integers."

    def test_invalid_json(self):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        report_conf_path = Path(
            __file__).parent / "jsons/reading_error/invalid_json.json"

        with pytest.raises(ValueError):
            report.build_report(report_conf_path, verbose=True)

    @pytest.mark.parametrize("report_file_path",
                             ["report.html", "report.md", "report.pdf"],
                             ids=["html", "md", "pdf"])
    def test__get_report_path_and_suffix(self, report_file_path):

        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        suffix, path = report._Report__get_report_path_and_suffix(
            report_file_path)

        assert suffix == report_file_path.split(".")[-1]
        assert path == Path.cwd() / report_file_path

    def test__get_report_path_and_suffix_invalid_format(self):

        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        # report file path without extension
        with pytest.raises(ValueError) as err:
            report._Report__get_report_path_and_suffix("foo")

        assert str(err.value).startswith(
            "Report path must include the file type suffix") is True

        # unsupported file format
        with pytest.raises(ValueError) as err:
            report._Report__get_report_path_and_suffix("foo.bar")

        assert str(err.value).startswith(
            "The only supported report formats are") is True

    def test__get_report_path_and_suffix_no_report_path(self):

        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        # no report_file_path is passed
        with pytest.warns(UserWarning) as ws:
            report._Report__get_report_path_and_suffix()

        ws = [w for w in ws if w._category_name == 'UserWarning']
        assert len(ws) == 1
        assert ws[0].message.args[0] == "No report file path defined. " \
                                        "Using default (report.html)."

    def test__get_report_path_and_suffix_wrong_report_path_type(self):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        with pytest.raises(TypeError) as err:
            report._Report__get_report_path_and_suffix(123)

        assert str(err.value) == "Report path must be a string or a Path " \
                                 "object."

    def test_report_config_file_not_found(self):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        with pytest.raises(FileNotFoundError) as err:
            report.build_report("not_found.json", True)

        assert str(err.value) == "Report configuration file not found."

    def test_report_config_file_not_json(self):
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        invalid_config_path = Path(__file__).parent / "jsons/reading_error" \
                                                      "/invalid_config_fmt.txt"
        with pytest.raises(ValueError) as err:
            report.build_report(invalid_config_path, True)

        assert str(err.value).startswith(
            "Report configuration file must be a JSON") is True

        # config file path neither a string nor Path
        with pytest.raises(ValueError) as err:
            report.build_report(3, True)

        assert str(err.value) == "report_conf must be a str, Path or dict."

    def test_report_to_file(self, tmpdir, mocker):
        import sdqc
        from sdqc.reports import Report

        # TODO: move the instantiation of the Report object to a fixture
        out = {"file": [["file1", "file2"]],
               "sheet": [["sheet1", "sheet2"]],
               "cell": [["A1", "A2"]],
               "check_target": ["data"]}
        report = Report(out)

        # the report.buffer is an empty StringIO object, therefore the user
        # should provide it through the report argument
        with pytest.raises(ValueError) as err:
            report.report_to_file(report_filename=tmpdir/"report.html")

        assert str(err.value) == "No report provided."

        # try with real report file
        path = Path(__file__).parent.joinpath('models',
                                              'missing_values')
        report_path = Path(tmpdir).joinpath("report.html")

        report_config_path = Path(__file__).parent.parent.joinpath(
            'sdqc/default-report-conf.json')

        # Load from mdl file
        objs = sdqc.check(path.joinpath('missing_values.mdl'),
                          path.joinpath('missing_values.ini'),
                          output='report',
                          report_config=report_config_path,
                          report_filename=report_path,
                          verbose=True)

        assert report_path.exists()

        message = "No report provided. Using that in the buffer."
        # use the same report result and write it to docx
        report_path = Path(tmpdir).joinpath("report.docx")
        with pytest.warns(UserWarning, match=message):
            objs.report_to_file(report_filename=report_path)
        assert report_path.exists()

        # use the same report result and write it to pdf
        report_path = Path(tmpdir).joinpath("report.pdf")
        with pytest.warns(UserWarning, match=message):
            objs.report_to_file(report_filename=report_path)
        assert report_path.exists()

        # use the same report result and write it to markdown
        report_path = Path(tmpdir).joinpath("report.md")
        with pytest.warns(UserWarning, match=message):
            objs.report_to_file(report_filename=report_path)
        assert report_path.exists()

        # mocking the pypandoc get_pandoc_version function to always return
        # 2.16
        message_version = f"pandoc version in your system is 2.16. " \
                          "Please install pandoc version 2.17 or above."

        with pytest.warns(UserWarning, match=message_version):
            mocker.patch('pypandoc.get_pandoc_version', lambda: "2.16")
            objs.report_to_file(report_filename=report_path)


