import pypandoc
from pypandoc.pandoc_download import download_pandoc


if __name__ == "__main__":
    # this will download the latest version of pandoc
    download_pandoc()