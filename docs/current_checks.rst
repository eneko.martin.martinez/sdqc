Currently implemented data quality checks
=========================================

A list of available checks is shown below:

.. csv-table::
   :file: tables/check_list.csv
   :header-rows: 1

\* *completness* argument is only used for *dataseries* calling *missing_values_data*

\*\* the check for missing values is always passed over series values as the missing values in the series dimesion have to be removed before passing other tests.

Information about each check argument is shown in the table below:

.. csv-table::
   :file: tables/arg_list.csv
   :header-rows: 1

