Installation
============

Installing using pip
--------------------

To install the SDQC package from the Python package index use the following command:

.. code-block:: bash

   pip install sdqc

Installing from source
----------------------
To install from source, clone the project with git:

.. code-block:: bash

   git clone https://gitlab.com/eneko.martin.martinez/sdqc

or download the `latest version from GitLab <https://gitlab.com/eneko.martin.martinez/sdqc>`_.

From the project's main directory, use the following command to install it:

.. code-block:: bash

   python setup.py install

Required Dependencies
---------------------
SDQC requires Python 3.7+, PySD 3.8+, pypandoc (and pandoc 2.17+), netCDF4 (1.5 for Python 3.7 in Windows and 1.6+ for all other cases), and importlib-metadata 2.0 (only for Python 3.7).


If not installed, PySD should be built automatically if you are installing via `pip` or from source.
