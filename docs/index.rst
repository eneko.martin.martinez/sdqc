.. sdqc documentation master file, created by
   sphinx-quickstart on Tue Feb 16 13:30:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the SDQC library documentation!
==========================================

*System Dynamics Quality Check (SDQC)*: run quality checks over Vensim models input data files.

|made-with-sphinx-doc|
|docs|
|PyPI license|
|PyPI package|
|PyPI status|
|PyPI pyversions|
|pipeline|
|coverage|

.. |made-with-sphinx-doc| image:: https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg
   :target: https://www.sphinx-doc.org/

.. |docs| image:: https://readthedocs.org/projects/sdqc/badge/?version=latest
   :target: https://sdqc.readthedocs.io/en/latest/?badge=latest

.. |PyPI license| image:: https://img.shields.io/pypi/l/sdqc.svg
   :target: https://gitlab.com/eneko.martin.martinez/sdqc/-/blob/master/LICENSE

.. |PyPI package| image:: https://badge.fury.io/py/sdqc.svg
    :target: https://badge.fury.io/py/sdqc

.. |PyPI pyversions| image:: https://img.shields.io/pypi/pyversions/sdqc.svg
   :target: https://pypi.python.org/pypi/sdqc/

.. |PyPI status| image:: https://img.shields.io/pypi/status/sdqc.svg
   :target: https://pypi.python.org/pypi/sdqc/

.. |pipeline| image:: https://gitlab.com/eneko.martin.martinez/sdqc/badges/master/pipeline.svg
   :target: https://gitlab.com/eneko.martin.martinez/sdqc/

.. |coverage| image:: https://gitlab.com/eneko.martin.martinez/sdqc/badges/master/coverage.svg
   :target: https://gitlab.com/eneko.martin.martinez/sdqc/



System Dynamics models are generally data intensive. Large datasets are difficult to maintain and are commonly plagued with issues that affect their quality. When low quality data is fed to SD models, the issues propagate to their outputs.

This library facilitates the task of loading input data to System Dynamic models from external files (spreadsheets), running common data quality checks on it (e.g. outlier detection, missing values, etc.) and generating data quality reports.

This project was originally thought as an extension to the `PySD <https://github.com/SDXorg/pysd>`_ library, and in fact, it uses it to load the model input data by parsing either the Vensim model file (.mdl) or the Python translation (using PySD). Additionally, the library also allows to load data from external data files by reading the location of each dataset from a user defined dictionary.

Vensim is a well known platform to build SD models. However, its flexibility when it comes to loading external data also comes at a price. Indeed, when defining calls to external data from Vensim, it is surprisingly easy to introduce errors that generally go unnoticed by the software and that may lead to unexpected model results. If addition, the root of such unexpected results is generally painful to track down. Accordingly, **this library should be of particular interest to Vensim users who work on large models** and want to make sure that what they feed to their model is what they expect.

The code for SDQC is available `here <https://gitlab.com/eneko.martin.martinez/sdqc>`_. If you find a bug, or if you wish to requuest a new feature, please use `GitLab's issue tracker <https://gitlab.com/eneko.martin.martinez/sdqc/-/issues>`_. For contributions see the :doc:`Development <development>` section.

Contents:
---------

.. toctree::
   :maxdepth: 2

   installation
   usage
   current_checks
   api
   report_generation
   development




Aditional Resources
-------------------
PySD docs
^^^^^^^^^
PySD documentation is available at:
http://pysd.readthedocs.org/


Acknowledgmentes
----------------
This library was originally developed for `H2020 LOCOMOTION <https://www.locomotion-h2020.eu>`_ project by `@eneko.martin.martinez <https://gitlab.com/eneko.martin.martinez>`_ at `Centre de Recerca Ecològica i Aplicacions Forestals (CREAF) <http://www.creaf.cat>`_.

This project has received funding from the European Union’s Horizon 2020
research and innovation programme under grant agreement No. 821105.
